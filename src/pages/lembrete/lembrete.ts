import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController, ViewController } from 'ionic-angular';
import firebase from 'firebase'

@IonicPage()
@Component({
  selector: 'page-lembrete',
  templateUrl: 'lembrete.html',
})
export class LembretePage {
  
  
  userId: string  = firebase.auth().currentUser.uid;
  arrayClientes: any [] = [];
  arrayClienteDados:any [] = [];
  
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public modalCtrl: ModalController,
    public viewCtrl: ViewController) {
      this.getLembrete()
    }
    
    criarLembrete(usuario){
      const modal = this.modalCtrl.create('ModalLembretePage', {"usuario":this.arrayClientes[4] });
      modal.present();
    }

    getLembrete(){
      var array: any []
      var userId = firebase.auth().currentUser.uid;
      firebase.database().ref('/teste-advogado-cliente/' + userId).once('value', ((docs) => {
        docs.forEach((doc)=>{
          this.arrayClientes.push(doc.val())
        })
      })).then(() => {
        console.log(this.arrayClientes[4])
      })
   
    }
    
    // getIdCliente(){
    //   console.log('aqui')
    //   //
    //   //Adquirindo as dados de id que estão presentes no nó 'teste-advogado-cliente
    //   //
    //   firebase.database().ref('teste-advogado-cliente').child(this.userId)
    //   .once('value', ((docs) => {
    //     docs.forEach((doc) => {
    //       this.arrayClientes.push(doc.val())
    //       console.log(doc.val())
    //     })
    //   }))
    //   if(this.arrayClientes[2] == undefined){
    //     console.log( 'erro' )
    //   }else{
    //     console.log(this.arrayClientes[2])
    //     firebase.database().ref('usuario').child(this.arrayClientes[2])
    //     .once('value', ((docs) => { 
    //       Object.keys(docs.val()).forEach(key => {
    //         this.arrayClienteDados.push(docs.val()[key])
    //         console.log(this.arrayClientes)
    //     });
    //     }))
    //   }
    // }
    fechar(){
      this.viewCtrl.dismiss();
    }
  }
  